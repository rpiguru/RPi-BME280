# A Simple Project with RPi, BME280, and Relays.


## Components

- Raspberry Pi Zero W
    
    https://www.amazon.com/Raspberry-Starter-Power-Supply-Premium/dp/B0748MPQT4/ref=sr_1_6?s=electronics&ie=UTF8&qid=1518041724&sr=1-6&keywords=raspberry+pi+zero

- BME280 Breakout Board
    
    https://www.amazon.com/Diymall-Pressure-Temperature-Sensor-Arduino/dp/B0118XCKTG/ref=sr_1_1?ie=UTF8&qid=1518041218&sr=8-1&keywords=bme280
    
- 4-Channel Relay Board.

    https://www.amazon.com/RobotDyn-Module-channel-operation-110VAC/dp/B072TTXGZ7/ref=sr_1_1?s=electronics&ie=UTF8&qid=1518041929&sr=1-1&keywords=110vac%2Brelay%2Bmodule&th=1
    
## Connection

RPi Zero Pinout: http://raspi.tv/wp-content/uploads/2015/11/Pi-Zero-Portsplus_1500.jpg

- RPi and BME280
    
    | **RPi** | **BME280**|
    |:-------:|:---------:|
    | 3V3     | 3V3       |
    | GND     | GND       |
    | GP2     | SDA       |
    | GP3     | SCL       |

- RPi and Relay Module
    
    | **RPi** | **Relay** |
    |:-------:|:---------:|
    | 5V      | VCC               |
    | GND     | GND               |
    | GND     | SGND              |
    | GP6     | IN1 (Humidifier)  |
    | GP13    | IN2 (Dehumidifier)|
    | GP19    | IN3 (Cooling)     |
    | GP26    | IN4 (Heating)     |


## Scenario
    
1. Humidifier: Relay gives 110v to humidifier at 60% relative humidity and turns off at 62% relative humidity.
2. Dehumidifier: Relay gives 110v to fan at 65% relative humidity and turns off at 63% relative humidity.
3. Cooling: Relay gives 110v to a refrigerator at 55 degrees F and turns off at 53 degrees F. 
4. Heating: Relay gives 110v to a small space heater at 50 degrees F and turns off at 52 degrees F.

## Installation

- Install Raspbian **Stretch Lite** from [here](https://www.raspberrypi.org/downloads/raspbian/)

- Configure I2C on RPi
    
    https://learn.adafruit.com/adafruits-raspberry-pi-lesson-4-gpio-setup/configuring-i2c
    
- Install everything
        
        bash install.sh

- Now, reboot!
