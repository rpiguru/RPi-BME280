import datetime
import os
import random
import time


def is_rpi():
    return os.name == 'posix' and 'arm' in os.uname()[4]


if is_rpi():
    from Adafruit_BME280 import *


def detect_bme280_address():
    pipe = os.popen('i2cdetect -y 1')
    data = pipe.read().strip()
    pipe.close()
    if '76' in data:
        return 0x76
    elif '77' in data:
        return 0x77


def read_sensor_data(address=None):
    if address is None:
        address = detect_bme280_address()
    if is_rpi():
        try:
            sensor = BME280(t_mode=BME280_OSAMPLE_8, p_mode=BME280_OSAMPLE_8, h_mode=BME280_OSAMPLE_8, address=address)
            temp_c = sensor.read_temperature()
            temp_f = round(temp_c * 1.8 + 32, 1)
            pressure = sensor.read_pressure() / 100
            humidity = round(sensor.read_humidity(), 1)
        except Exception as e:
            print('Failed to read sensor data - {}'.format(e))
            return
    else:
        temp_c = random.randint(2000, 3000) / 100.
        temp_f = temp_c * 1.8 + 32
        pressure = 1000 + random.randint(100, 200) / 10.
        humidity = random.randint(4000, 7000) / 100.
    return dict(temp_c=temp_c, temp_f=temp_f, pressure=pressure, humidity=humidity)


if __name__ == '__main__':

    addr = detect_bme280_address()

    while True:
        s_time = time.time()
        try:
            _data = read_sensor_data(address=addr)
            print(datetime.datetime.now())
            print(_data)
            print('Elapsed: {}\n'.format(time.time() - s_time))
        except OSError:
            pass
        except KeyboardInterrupt:
            break
